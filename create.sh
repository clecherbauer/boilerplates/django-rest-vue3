#!/usr/bin/env bash
set -e

BASHOPS_VERSION="1.3.1"

for ARGUMENT in "$@"
do
   KEY=$(echo $ARGUMENT | cut -f1 -d=)

   KEY_LENGTH=${#KEY}
   VALUE="${ARGUMENT:$KEY_LENGTH+1}"

   export "$KEY"="$VALUE"
done

function help() {
    echo ""
    echo "Usage: $0 project_name=xyz"
    echo "Optional:"
    echo "target=/path/to/target"
    echo "force=true"
}

if [[ "$@" != *"project_name"* ]]; then
    echo "Please set a project_name!"
    help
    exit 1
fi

if [ $target == "" ]; then
  target="$PWD"
fi
project_path="$target/$project_name"

if [ ! -d "$target" ]; then echo "No such directory $target"; exit 1; fi
if [ -d "$project_path" ] && [ "$force" == "" ]; then echo "Project already exists!"; exit 1; fi

if [ "$force" != "" ]; then
  rm -Rf "$project_path"
fi

mkdir "$project_path"
cp -R "./src/." "$project_path"
grep -rl "#x#PROJECT_NAME#x#" $project_path | xargs sed -i "s/#x#PROJECT_NAME#x#/$project_name/g"

cd "$project_path" || exit 1
git init
git submodule add -b "$BASHOPS_VERSION" https://gitlab.com/clecherbauer/tools/bashops.git .devops/bashops

echo "Success: You new project was created at $project_path."
