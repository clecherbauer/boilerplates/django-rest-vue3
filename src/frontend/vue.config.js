const path = require('path')
module.exports = {
  lintOnSave: false,
  publicPath: '/',
  devServer: {
    host: '0.0.0.0',
    port: 8080,
    sockPort: 80,
    disableHostCheck: true,
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: '@import "@/scss/app.scss";',
      }
    },
  },
  chainWebpack: (config) => {
    console.log(config)
    config.resolve.alias.set('ui', path.resolve('src/components/ui'))

    const svgRule = config.module.rule('svg')

    svgRule.uses.clear()

    svgRule
      .use('vue-loader')
      .loader('vue-loader-v16') // or `vue-loader-v16` if you are using a preview support of Vue 3 in Vue CLI
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader')
  },
  pages: {
    index: {
      // entry for the page
      entry: 'src/main.js',
      // the source template
      template: 'public/index.html',
      // output as dist/index.html
      filename: 'index.html',
      // when using title option,
      // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
      title: '#x#PROJECT_NAME#x#',
      // chunks to include on this page, by default includes
      // extracted common chunks and vendor chunks.
      chunks: ['chunk-vendors', 'chunk-common', 'index'],
    },
  },
}
