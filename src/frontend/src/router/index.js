import { createRouter, createWebHistory } from 'vue-router'

import Home from '@/components/Home.vue'

let getRoutes = function () {
  return [
    {
      path: '/',
      component: Home
    }
  ]
}

const getRouter = function (auth) {
  return createRouter({ history: createWebHistory('/'), routes: getRoutes(auth) })
}

export default getRouter
