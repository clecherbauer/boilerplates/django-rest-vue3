import { createApp } from 'vue'
import App from './App.vue'

import getRouter from './router'

const router = getRouter()
const app = createApp(App)
app.use(router)
app.config.globalProperties.$appVersion = process.env.VUE_APP_DEPLOYMENT_VERSION
app.mount('#app')
