##x#PROJECT_NAME#x#


### Installation of Docker Dependencies
Execute this after cloning the Project

    ./dev.sh setup

### First Startup
    docker-compose build
    ./dev.sh init
    docker-compose up

### Update Containers and Dependencies (mostly needed after Branch-switching)
    docker-compose build
    ./dev.sh update


### Commands
#### API Management

Load Fixtures

    python manage.py load_all_fixtures


#### Python Code Syntax Linting / Fixing

    flake8 --config=tox.ini api

    autopep8 --in-place --recursive api

#### Javascript Code Syntax Linting / Fixing

    npx vue-cli-service lint

#### Docker Specific
Remove Docker Volumes

    docker-compose down -v
