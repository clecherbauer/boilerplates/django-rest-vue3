#!/usr/bin/env bash
# shellcheck disable=SC1091
# shellcheck disable=SC1090
# shellcheck disable=SC2001
set -e

source "$(dirname "$(readlink -f "$0")")/../../bashops/functions.sh"
BASHOPS_OVERWRITE="$(dirname "$(readlink -f "$0")")/../../bashops.sh"
if [ -f $BASHOPS_OVERWRITE ]; then
  source $BASHOPS_OVERWRITE
fi

NGINX_CONFIG_FILE="/etc/nginx/nginx.conf"
NGINX_VHOST_CONFIG_FILE="/etc/nginx/conf.d/default.conf"

cp /var/source/.devops/docker/proxy/config/nginx.conf "$NGINX_CONFIG_FILE"
cp /var/source/.devops/docker/proxy/config/default.conf "$NGINX_VHOST_CONFIG_FILE"

# mkdir -p /var/www/html
# cp /var/source/.deployment/docker/proxy/config/502.html /var/www/html
# cp /var/source/.deployment/docker/proxy/config/503.html /var/www/html

if isReviewInstance || isStagingInstance || isProductionInstance; then
    PROJECT_NAMESPACE="$(getProjectNamespace)"
    sed -i "s/###API-DNS###/api.$PROJECT_NAMESPACE/g" "$NGINX_VHOST_CONFIG_FILE"
    sed -i "s/###FRONTEND-DNS###/frontend.$PROJECT_NAMESPACE/g" "$NGINX_VHOST_CONFIG_FILE"
fi

if isLocalDevInstance; then
    sed -i "s/###API-DNS###/api/g" "$NGINX_VHOST_CONFIG_FILE"
    sed -i "s/###FRONTEND-DNS###/frontend/g" "$NGINX_VHOST_CONFIG_FILE"
fi

rm -Rf /var/source