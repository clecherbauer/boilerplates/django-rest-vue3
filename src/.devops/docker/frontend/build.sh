#!/usr/bin/env bash
# shellcheck disable=SC1091
# shellcheck disable=SC1090
# shellcheck disable=SC2001
# shellcheck disable=SC2038
set -e

source "$(dirname "$(readlink -f "$0")")/../../bashops/functions.sh"
BASHOPS_OVERWRITE="$(dirname "$(readlink -f "$0")")/../../bashops.sh"
if [ -f $BASHOPS_OVERWRITE ]; then
  source $BASHOPS_OVERWRITE
fi

SOURCE_ROOT="/var/source"
WEB_ROOT="/app"

export NODE_OPTIONS=--max_old_space_size=2048


mkdir -p /home/www-data/.config
chown -R www-data /home/www-data

npm install -g npx --force

if isReviewInstance || isStagingInstance || isProductionInstance; then
    export VUE_APP_DEPLOYMENT_VERSION="$VERSION"
    (
        cd "$SOURCE_ROOT/frontend" || exit
        npm install --unsafe-perm
        npm run build
        cp -r "dist" "$WEB_ROOT"
    )
    cp "$SOURCE_ROOT/.devops/docker/frontend/config/nginx.conf" /etc/nginx/nginx.conf
fi

if isLocalDevInstance; then
    modifyWwwDataUser
fi

#cleanup
rm -Rf "$SOURCE_ROOT"