#!/usr/bin/env bash
# shellcheck disable=SC1091
# shellcheck disable=SC1090
# shellcheck disable=SC2001
set -e

npm install -g npx @vue/cli --force
npm config set unsafe-perm true
