#!/usr/bin/env bash
# shellcheck disable=SC1091
# shellcheck disable=SC1090
# shellcheck disable=SC2001
set -e

SOURCE_ROOT="/var/source"
cp "$SOURCE_ROOT/.devops/docker/alias/python/config/entrypoint.sh" /usr/local/bin

apt-get -qq update
apt-get -qq install -y python3-dev graphviz libgraphviz-dev pkg-config libfontconfig

pip3 install flake8 autopep8 pygraphviz prospector prospector2html
