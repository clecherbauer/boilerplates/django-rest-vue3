#!/usr/bin/env bash
# shellcheck disable=SC1091
# shellcheck disable=SC1090
# shellcheck disable=SC2001
set -e

source "$(dirname "$(readlink -f "$0")")/../../bashops/functions.sh"
BASHOPS_OVERWRITE="$(dirname "$(readlink -f "$0")")/../../bashops.sh"
if [ -f $BASHOPS_OVERWRITE ]; then
  source $BASHOPS_OVERWRITE
fi

SOURCE_ROOT="/var/source"
WEB_ROOT="/app"

mkdir -p "$WEB_ROOT"
cp "$SOURCE_ROOT/.devops/docker/api/config/entrypoint.sh" /usr/local/bin
cp "$SOURCE_ROOT/api/manage.py" "$WEB_ROOT"

if ! isLocalDevInstance; then
    echo 'test'
    cp -r "$SOURCE_ROOT/api" "$WEB_ROOT"
    mkdir -p "$WEB_ROOT/api/media/tmp"
    mkdir -p "$WEB_ROOT/api/.pydeps"
    chown -R www-data:www-data "$WEB_ROOT"
    pip3 install --target "$WEB_ROOT/api/.pydeps" --upgrade -r "$SOURCE_ROOT/api/requirements.txt"
fi

if isLocalDevInstance; then
    modifyWwwDataUser
fi

rm -Rf "$SOURCE_ROOT"
