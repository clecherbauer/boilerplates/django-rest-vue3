#!/usr/bin/env bash
# shellcheck disable=SC2044
# shellcheck disable=SC2069
set -e

export PATH=$PATH:/app/api/.pydeps/bin

function waitForMysql () {
    MAX_TIMES=300
    TRIED_TIMES=0
    echo "Waiting for mysql"
    until mysqladmin ping -h "$MYSQL_HOST" &> /dev/null
    do
      echo -n -e ". "
      sleep 1
      if [ $((TRIED_TIMES++)) -gt $MAX_TIMES ]; then
        echo ">>> Error! Too many attempts to connect to $MYSQL_HOST"
        exit 1
      fi
    done
}

function migrateDatabase() {
    su www-data -s /bin/bash -c "python manage.py createcachetable"
    su www-data -s /bin/bash -c "python manage.py migrate"
}

function collectStaticFiles() {
    echo "Collect static files..."
    chown -Rf www-data /app
    su www-data -s /bin/bash -c "python manage.py collectstatic --noinput" 2>&1 >/dev/null
}

_ORIG_ENVIRONMENT="$ENVIRONMENT"
export ENVIRONMENT=boot

waitForMysql

if [ "$_ORIG_ENVIRONMENT" != "production" ]; then
    migrateDatabase
  collectStaticFiles
fi

export ENVIRONMENT="$_ORIG_ENVIRONMENT"

exec "$@"
