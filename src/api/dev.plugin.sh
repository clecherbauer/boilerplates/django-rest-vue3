function plugin_name() {
  echo "api"
}

function cleanup() {
    [ -d .pydeps ] || mkdir .pydeps
    rm -Rf .pydeps/*
}

function init() {
    update
}

function update() {
  cleanup
  pip3 install --target ".pydeps" --upgrade -r requirements.txt
}
