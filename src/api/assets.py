from django_assets import Bundle, register

supplier_report_css = Bundle(
    'https://fonts.googleapis.com/css?family=Work+Sans&display=swap',
    '/app/api/templates/styles/imports/_utility-classes.css',
    '/app/api/templates/styles/imports/_base.css',
    '/app/api/templates/styles/imports/_page-layout.css',
    '/app/api/templates/styles/imports/_section-header.css',
    '/app/api/templates/styles/imports/_chip-delta.css',
    '/app/api/templates/styles/imports/_icon-flag.css',
    '/app/api/templates/styles/supplier_report.css',
    output='/app/api/static/generated_styles/supplier_report.css'
)

register('supplier_report_css', supplier_report_css)

benchmark_report_css = Bundle(
    'https://fonts.googleapis.com/css?family=Work+Sans&display=swap',
    '/app/api/templates/styles/imports/_utility-classes.css',
    '/app/api/templates/styles/imports/_base.css',
    '/app/api/templates/styles/imports/_page-layout.css',
    '/app/api/templates/styles/imports/_section-header.css',
    '/app/api/templates/styles/imports/_chip-delta.css',
    '/app/api/templates/styles/imports/_icon-flag.css',
    '/app/api/templates/styles/benchmark_report.css',
    output='/app/api/static/generated_styles/benchmark_report.css'
)

register('benchmark_report_css', benchmark_report_css)


negotiation_report_css = Bundle(
    'https://fonts.googleapis.com/css?family=Work+Sans:400,500,600',
    '/app/api/templates/styles/imports/_utility-classes.css',
    '/app/api/templates/styles/imports/_base.css',
    '/app/api/templates/styles/imports/_page-layout.css',
    '/app/api/templates/styles/imports/_section-header.css',
    '/app/api/templates/styles/imports/_chip-delta-rating.css',
    '/app/api/templates/styles/imports/_legend-delta-rating.css',
    '/app/api/templates/styles/imports/_icon-flag.css',
    '/app/api/templates/styles/imports/_rate-history-list.css',
    '/app/api/templates/styles/imports/_rate-history-item.css',
    '/app/api/templates/styles/negotiation_report.css',
    output='/app/api/static/generated_styles/negotiation_report.css'
)

register('negotiation_report_css', negotiation_report_css)


risk_booklet_css = Bundle(
    'https://fonts.googleapis.com/css?family=Work+Sans:400,500,600',
    '/app/api/templates/styles/risk_booklet.css',
    '/app/api/templates/styles/imports/_utility-classes.css',
    '/app/api/templates/styles/imports/_base.css',
    '/app/api/templates/styles/imports/_page-layout.css',
    '/app/api/templates/styles/imports/_chip-risk-rating.css',
    output='/app/api/static/generated_styles/risk_booklet.css'

)

register('risk_booklet_css', risk_booklet_css)
