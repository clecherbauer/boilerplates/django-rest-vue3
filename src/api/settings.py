import os
import sys
from os.path import join

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SETTINGS_PATH = os.path.normpath(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get('ENVIRONMENT') in ['development', 'review', 'staging']
TESTING = len(sys.argv) > 1 and sys.argv[1] == 'test' or os.environ.get('ENVIRONMENT') == 'test'
CLI = len(sys.argv) >= 1 and sys.argv[0] == 'manage.py' or os.environ.get('ENVIRONMENT') == 'cli'
BOOTING = os.environ.get('ENVIRONMENT') == 'boot'

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'api',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.staticfiles',
    'rest_framework',
    'drf_spectacular',
    'drf_spectacular_sidecar',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware'
]

ROOT_URLCONF = 'api.urls'
URI_PREFIX = 'api/v1'
WSGI_APPLICATION = 'api.wsgi.api'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'DIRS': [os.path.join(SETTINGS_PATH, 'templates')],
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': os.environ.get('MYSQL_HOST'),
        'NAME': os.environ.get('MYSQL_DATABASE'),
        'USER': os.environ.get('MYSQL_USER') if not TESTING else 'root',
        'PASSWORD': os.environ.get('MYSQL_PASSWORD'),
        'PORT': 3306,
        'OPTIONS': {
            'sql_mode': 'STRICT_TRANS_TABLES',
            'charset': 'utf8mb4',
            'use_unicode': True
        }
    }
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/
DATE_INPUT_FORMATS = ('%d/%m/%Y', '%d-%m-%Y', '%Y-%m-%d', '%d.%m.%Y')
LANGUAGE_CODE = 'de-ch'
TIME_ZONE = 'Europe/Zurich'
USE_I18N = True
USE_L10N = True
USE_TZ = True


def gettext(s):
    return s


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
STATIC_URL = '/' + URI_PREFIX + '/static/'
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')

if TESTING:
    MEDIA_URL = '/api/media/test/'
    MEDIA_ROOT = os.path.join(PROJECT_ROOT, join('media', 'test'))
    MEDIA_LIVE_ROOT = os.path.join(PROJECT_ROOT, 'media')
else:
    MEDIA_URL = '/api/media/'
    MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')

# Media Fixtures
MEDIA_FIXTURE_FOLDERNAME = 'medias'


# Enable HTTP_X_FORWARDED_PROTO if available
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
USE_X_FORWARDED_HOST = True

FORCE_SCRIPT_NAME = '/' + URI_PREFIX

REST_FRAMEWORK = {
    'DEFAULT_SCHEMA_CLASS': 'drf_spectacular.openapi.AutoSchema',
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 10,
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': [],
    'DEFAULT_FILTER_BACKENDS': [],
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
}

SPECTACULAR_SETTINGS = {
    'TITLE': '#x#PROJECT_NAME#x# API',
    'DESCRIPTION': '',
    'VERSION': '1.0.0',
    'SWAGGER_UI_DIST': 'SIDECAR',  # shorthand to use the sidecar instead
    'SWAGGER_UI_FAVICON_HREF': 'SIDECAR',
    'SWAGGER_UI_SETTINGS': {
        'url': '/' + URI_PREFIX + '/schema/',
        # absolute path 'url': '/path/to/schema/',
        # relative path
    }
}

GRAPH_MODELS = {
    'app_labels': ["api"],
}

API_TEMP_DIR_PATH = '/app/api/media/tmp'
APP_LABEL = 'api'
