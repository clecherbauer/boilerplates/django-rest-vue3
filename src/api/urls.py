from api.settings import URI_PREFIX
from django.urls import path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework import routers


class OptionalSlashRouter(routers.SimpleRouter):

    def __init__(self):
        super().__init__()
        self.trailing_slash = '/?'


urlpatterns = [
    path(URI_PREFIX + '/schema/', SpectacularAPIView.as_view(), name='schema'),
    path(URI_PREFIX + '/schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
]
